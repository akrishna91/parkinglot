package com.krishna.model;

import lombok.Builder;
import lombok.Data;
import lombok.Setter;

/**
 * @author krishna.tripathi on 06/03/20
 */
@Data
@Builder
@Setter
public class Vehicle {

    public enum VehicleType {

        CAR, TRUCK, BIKE
    }

    private String idNumber;
    private VehicleType vehicleType;
    private Ticket ticket;
    private Spot spot;
    private String parkingFloorName;

    public void assignTicket(Ticket ticket){
        this.setTicket(ticket);
    }


}

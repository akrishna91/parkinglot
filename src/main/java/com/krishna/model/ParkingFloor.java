package com.krishna.model;

import com.krishna.utils.TypeUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Parking floor
 * @author krishna.tripathi on 06/03/20
 */
@Data
public class ParkingFloor {

    String name;
    private Map<Spot.SlotType, List<Spot>> spots = new HashMap<>();
    private Map<Spot.SlotType, List<Spot>> assignedSpots = new HashMap<>();


    public ParkingFloor(String name) {
        this.name = name;
    }


    /**
     * adds all the spots in the list of spots
     * @param spots
     */
    public void addSpots(List<Spot> spots){

        spots.stream().forEach(spot -> {
            addSpot(spot);
        });

    }

    /**
     * adds the spot in list of spots
     * @param spot
     */
    public void addSpot(Spot spot){
        switch (spot.getSlotType()){
            case LARGE:
                spots.computeIfAbsent(Spot.SlotType.LARGE, slotType -> new ArrayList<>()).add(spot);
                break;

            case COMPACT:
                spots.computeIfAbsent(Spot.SlotType.COMPACT, slotType -> new ArrayList<>()).add(spot);
                break;

            case TWO_WHEELER:
                spots.computeIfAbsent(Spot.SlotType.TWO_WHEELER, slotType -> new ArrayList<>()).add(spot);
                break;
            default:
                System.out.println("wrong type");
        }


    }

    /**
     * assign vehicle on the given slot type
     * @param slotType
     * @param vehicle
     */
    public void assignVehicle(Spot.SlotType slotType, Vehicle vehicle){
        if (!spots.get(slotType).isEmpty()){
            Spot spot = spots.get(slotType).remove(spots.get(slotType).size()-1);
            spot.setFree(false);
            spot.setVehicle(vehicle);
            vehicle.setSpot(spot);
            vehicle.setParkingFloorName(this.name);
            assignedSpots.computeIfAbsent(slotType, slotType1 -> new ArrayList<>()).add(spot);
        } else {
            System.out.println(vehicle + " cannot be assigned");
        }
    }

    /**
     * assign vehicle on a free slot
     * @param vehicle
     */
    public void assignVehicle(Vehicle vehicle){
        Spot.SlotType slotType = TypeUtils.getVehicleToSlot(vehicle.getVehicleType());
        assignVehicle(slotType, vehicle);
    }


    /**
     *
     * @param slotType
     * @return
     */

    public boolean isFull(Spot.SlotType slotType){

        return spots.get(slotType).size() == 0;
    }

    /**
     * returns a free spot on the floor,
     * returns null if there is no free spot
     * @param slotType
     * @return
     */
    public Spot getFreeSpots(Spot.SlotType slotType){
        if (!isFull(slotType)){
            return spots.get(slotType).remove(spots.get(slotType).size()-1);
        }
        return null;
    }

    /**
     * free the spot at time vacating time
     * @param vacatingTime
     */
    public void vacateSpot(Vehicle vehicle, long vacatingTime){
        Spot spot = vehicle.getSpot();
        if (assignedSpots.getOrDefault(spot.getSlotType(), new ArrayList<>()).remove(spot)){
            spot.setFree(true);
            vehicle.getTicket().setPaidAt(vacatingTime);
            System.out.println("vehicle " + vehicle.getIdNumber() + " paid "+ vehicle.getTicket().getPrice()
                    + ". ticket number: " + vehicle.getTicket().getTicketNumber());
            spot.setVehicle(null);
            spots.get(spot.getSlotType()).add(spot);
        } else {
            throw new RuntimeException("spot is already free");
        }

    }


}

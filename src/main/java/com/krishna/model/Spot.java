package com.krishna.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @author krishna.tripathi on 06/03/20
 */
@Data
@AllArgsConstructor
public class Spot {

    public Spot(Integer id, boolean isFree, SlotType slotType) {
        this.id = id;
        this.isFree = isFree;
        this.slotType = slotType;
    }

    public enum SlotType {

        LARGE, COMPACT, TWO_WHEELER;
    }

    private Integer id;
    private boolean isFree;
    private SlotType slotType;
    private Vehicle vehicle;


}



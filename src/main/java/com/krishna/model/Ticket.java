package com.krishna.model;

import lombok.Data;

import java.util.Date;

/**
 * @author krishna.tripathi on 06/03/20
 */
@Data
public class Ticket {

    private long ticketNumber = 0;
    private long issuedAt;
    private long paidAt;
    private double price = 0;

    public Ticket(long ticketNumber, long issuedAt) {
        this.ticketNumber = ticketNumber;
        this.issuedAt = issuedAt;
    }

    /**
     * TODO can be based on {@link com.krishna.model.Spot.SlotType}
     * @return
     */
    public double getPrice(){
        long hours = paidAt - issuedAt;

        price += 50;

        hours -= 1;

        if (hours > 0){
            price += 30;
        }

        hours -= 1;

        if (hours > 0){
            price += 20*hours;
        }

        return price;

    }
}

package com.krishna.model;

import com.krishna.utils.TypeUtils;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * holds whole parking structure
 * @author krishna.tripathi on 06/03/20
 */
@Data
public class ParkingLot {

    private String id;
    private Map<String, ParkingFloor> parkingFloors = new HashMap<>();
    private static ParkingLot parkingLot = null;
    private static int ticketNumber = 0;

    private ParkingLot() {

    }

    public static ParkingLot getInstance(){

        if (parkingLot == null){
            parkingLot = new ParkingLot();
        }
        return parkingLot;
    }



    /**
     *
     * @param parkingFloor
     */
    public void addFloors(ParkingFloor parkingFloor){
        this.parkingFloors.put(parkingFloor.getName(), parkingFloor);
    }

    /**
     *
     * @return
     */
    public boolean isFull(Spot.SlotType slotType){

        for (ParkingFloor parkingFloor : parkingFloors.values()) {
            if (!parkingFloor.isFull(slotType)){
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @return
     */
    public boolean isFull(Vehicle.VehicleType vehicleType){
        Spot.SlotType slotType = TypeUtils.getVehicleToSlot(vehicleType);
        if (slotType ==  null){
            return false;
        }
        return isFull(slotType);
    }

    /**
     * returns free floor for given slot
     * @param slotType
     * @return
     */
    public ParkingFloor getFreeFloor(Spot.SlotType slotType){
        if (slotType == null){
            return null;
        }
        return parkingFloors.entrySet().stream().filter(kV -> !kV.getValue().isFull(slotType)).findFirst().get().getValue();
    }

    public void displayFreeSlots(){
        parkingFloors.forEach((name, parkingFloor) -> {
            System.out.println("--------------------------------------");
            System.out.println(parkingFloor.getName() + ":");

            parkingFloor.getSpots().forEach((slotType, spots) -> {
                System.out.println(slotType + " type of free spots:" + spots.size());
            });
        });

        System.out.println("********************************************");

    }

}

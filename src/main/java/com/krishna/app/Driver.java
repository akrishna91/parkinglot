package com.krishna.app;

import com.krishna.model.*;
import com.krishna.utils.IdGenerator;
import com.krishna.utils.TypeUtils;

/**
 * @author krishna.tripathi on 06/03/20
 */
public class Driver {

    public static void main(String[] args) {

        ParkingLot parkingLot = ParkingLot.getInstance();

        ParkingFloor parkingFloor1 = new ParkingFloor("floor1");
        parkingFloor1.addSpot(new Spot(1, true, Spot.SlotType.COMPACT));
        parkingFloor1.addSpot(new Spot(2, true, Spot.SlotType.LARGE));
        parkingFloor1.addSpot(new Spot(3, true, Spot.SlotType.TWO_WHEELER));
        parkingFloor1.addSpot(new Spot(4, true, Spot.SlotType.COMPACT));


        ParkingFloor parkingFloor2 = new ParkingFloor("floor2");
        parkingFloor2.addSpot(new Spot(1, true, Spot.SlotType.COMPACT));
        parkingFloor2.addSpot(new Spot(2, true, Spot.SlotType.LARGE));
        parkingFloor2.addSpot(new Spot(3, true, Spot.SlotType.TWO_WHEELER));
        parkingFloor2.addSpot(new Spot(4, true, Spot.SlotType.COMPACT));

        parkingLot.addFloors(parkingFloor1);
        parkingLot.addFloors(parkingFloor2);

        Vehicle vehicle1 = Vehicle.builder().vehicleType(Vehicle.VehicleType.TRUCK).idNumber("1").build();
        Vehicle vehicle2 = Vehicle.builder().vehicleType(Vehicle.VehicleType.TRUCK).idNumber("2").build();
        Vehicle vehicle3 = Vehicle.builder().vehicleType(Vehicle.VehicleType.BIKE).idNumber("3").build();
        Vehicle vehicle4 = Vehicle.builder().vehicleType(Vehicle.VehicleType.CAR).idNumber("4").build();

        parkingLot.displayFreeSlots();
        long enterTime1 = 1;
        assignVehicle(enterTime1, parkingLot, vehicle1);
        parkingLot.displayFreeSlots();
        assignVehicle(enterTime1, parkingLot, vehicle2);
        parkingLot.displayFreeSlots();
        assignVehicle(enterTime1, parkingLot, vehicle3);
        parkingLot.displayFreeSlots();

        vacateVehicle(parkingLot, vehicle2, 2);
        parkingLot.displayFreeSlots();

        vacateVehicle(parkingLot, vehicle1, 3);
        parkingLot.displayFreeSlots();

        ;


    }

    /**
     * vacate vehicle at parking lot
     * @param parkingLot
     * @param vehicle
     */
    private static void vacateVehicle(ParkingLot parkingLot, Vehicle vehicle, long vacateTime) {

        ParkingFloor parkingFloor = parkingLot.getParkingFloors().get(vehicle.getParkingFloorName());

        parkingFloor.vacateSpot(vehicle, vacateTime);

    }

    /**
     * assignes vehicle to the floor
     * @param enterTime1
     * @param parkingLot
     * @param vehicle
     */
    private static void assignVehicle(long enterTime1, ParkingLot parkingLot, Vehicle vehicle) {
        if (!parkingLot.isFull(vehicle.getVehicleType())){
            Spot.SlotType slotType = TypeUtils.getVehicleToSlot(vehicle.getVehicleType());
            ParkingFloor freeFloor = parkingLot.getFreeFloor(slotType);
            if (freeFloor != null){

                Ticket ticket = new Ticket(IdGenerator.getTicketNumber(), enterTime1);
                vehicle.setTicket(ticket);
                freeFloor.assignVehicle(vehicle);
            }
            else {
                System.out.println(vehicle.getVehicleType() + " with id " + vehicle.getIdNumber() + " cannot be assigned!" );
            }
        }
        else {
            System.out.println(vehicle.getVehicleType() + " with id " + vehicle.getIdNumber() + " cannot be assigned!" );
        }
    }
}

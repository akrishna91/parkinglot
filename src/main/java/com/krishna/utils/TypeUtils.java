package com.krishna.utils;

import com.krishna.model.Spot;
import com.krishna.model.Vehicle;

/**
 * @author krishna.tripathi on 06/03/20
 */
public class TypeUtils {

    /**
     * maps vehicleType to slotType
     * @param vehicleType
     * @return
     */
    public static Spot.SlotType getVehicleToSlot(Vehicle.VehicleType vehicleType){
        switch (vehicleType){
            case TRUCK:
                return Spot.SlotType.LARGE;
            case BIKE:
                return Spot.SlotType.TWO_WHEELER;
            case CAR:
                return Spot.SlotType.COMPACT;

            default:
                System.out.println("wrong type");
                return null;
        }
    }
}

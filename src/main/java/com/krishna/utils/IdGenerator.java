package com.krishna.utils;

/**
 * generates random id for different purposes
 * @author krishna.tripathi on 06/03/20
 */
public class IdGenerator {

    private static long id;


    public static long getTicketNumber(){
        return ++id;
    }
}
